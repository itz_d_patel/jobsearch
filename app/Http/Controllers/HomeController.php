<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Job;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('worker.home');
    }

    public function jobList()
    {
        return view('worker.joblist');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function jobSearch(Request $request)
    {
        $jobs = Job::with('user');

        if ($request->searchText != '') {
            $jobs->where('title', 'LIKE', '%' . $request->searchText . '%');
        }

        if ($request->jobtype != '') {
            $jobs->whereIn('jobtype', $request->jobtype);
        }

        $jobs = $jobs->get();
        
        return view('worker.job_list')->with(['jobs' => $jobs]);
    }
}
