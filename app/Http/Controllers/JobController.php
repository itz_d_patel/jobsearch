<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Job;
use App\User;

class JobController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $jobs = Job::where('user_id', Auth::guard()->user()->id)->get();
        // $workers = User::where('usertype', 'worker')->get();
        // return view('job.home')->with(['jobs' => $jobs, 'workers' => $workers]);
        return view('job.home');
    }

    public function workerList()
    {
        return view('job.workerlist');
    }

    public function payment()
    {
        return view('job.payment');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function workerSearch(Request $request)
    {
        $workers = User::where('usertype', 'worker');

        if ($request->text != '') {
            $workers->where('name', 'LIKE', '%' . $request->text . '%');
        }

        $workers = $workers->get();
        
        return view('job.worker_list')->with(['workers' => $workers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('job.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'description' => 'required',
            'technology' => 'required',
            'jobtype' => 'required',
            'budget' => 'required',
            'duration' => 'required',
        ];

        $this->validateForm($request->all(), $rules);

        $job = new Job();
        $job->user_id = Auth::guard()->user()->id;
        $job->title = $request->title;
        $job->description = $request->description;
        $job->technology = $request->technology;
        $job->jobtype = $request->jobtype;
        $job->budget = $request->budget;
        $job->duration = $request->duration;
        $job->save();

        // flash('Job added successfully.')->success();
        $request->session()->flash('status', 'Job was created successfully.');
        return redirect()->route('job.index');


    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return view('job.home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
