<?php

namespace App\Http\Middleware;

use Closure;

class IsWorker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && $request->user()->usertype != 'worker')
        {
            return redirect()->route('login');
        }
        return $next($request);
    }
}
