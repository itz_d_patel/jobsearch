<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    // $fillable = [];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
