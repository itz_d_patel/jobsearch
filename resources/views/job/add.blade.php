@extends('layouts.app')

@section('content')
<div class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_1.jpg');" data-stellar-background-ratio="0.5">
  <div class="overlay"></div>
  <div class="container">
    <div class="row no-gutters slider-text align-items-end justify-content-start">
      <div class="col-md-12 ftco-animate text-center mb-5">
        <!-- <p class="breadcrumbs mb-0"><span class="mr-3"><a href="index.html">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>About</span></p> -->
        <h1 class="mb-3 bread">Add Your Job</h1>
      </div>
    </div>
  </div>
</div>
<section class="ftco-section ftco-candidates ftco-candidates-2 bg-light">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 pr-lg-4">
                <div class="row">
                    <div class="col-md-12">
                        <form method="POST" class="bg-white p-5 contact-form" action="{{ route('job.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Job Title') }}</label>

                            <div class="col-md-6">
                                <input type="text" id="title" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" required>

                                @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Job Description') }}</label>

                            <div class="col-md-6">
                                <textarea id="description" rows="3" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}" required autocomplete="description" autofocus></textarea>

                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="technology" class="col-md-4 col-form-label text-md-right">{{ __('Technology') }}</label>

                            <div class="col-md-6">
                                <select id="technology" class="form-control @error('technology') is-invalid @enderror" name="technology" value="{{ old('technology') }}" required>
                                    <option value="">Select Technology</option>
                                    <option value="android">Android</option>
                                    <option value="c">C</option>
                                    <option value="ios">iOS</option>
                                    <option value="php">PHP</option>
                                    <option value="python">Python</option>
                                </select>

                                @error('technology')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="jobtype" class="col-md-4 col-form-label text-md-right">{{ __('Job Type') }}</label>

                            <div class="col-md-6">
                                <select id="jobtype" class="form-control @error('jobtype') is-invalid @enderror" name="jobtype" value="{{ old('jobtype') }}" required>
                                    <option value="">Select Job Type</option>
                                    <option value="parttime">parttime</option>
                                    <option value="fulltime">fulltime</option>
                                    <option value="intership">intership</option>
                                    <option value="temporary">temporary</option>
                                    <option value="freelance">freelance</option>
                                    <option value="fixed">fixed</option>
                                </select>

                                @error('jobtype')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="budget" class="col-md-4 col-form-label text-md-right">{{ __('Budget') }}</label>

                            <div class="col-md-6 input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">$</span>
                                </div>
                                <input id="budget" type="number" class="form-control @error('budget') is-invalid @enderror" name="budget" required autocomplete="current-budget" min="10" max="1000">

                                @error('budget')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="duration" class="col-md-4 col-form-label text-md-right">{{ __('Duration') }}</label>

                            <div class="col-md-6 input-group">
                                <input id="duration" type="number" class="form-control @error('duration') is-invalid @enderror" name="duration" required autocomplete="current-duration" min="1" max="30">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Days</span>
                                </div>

                                @error('duration')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-outline-primary">
                                    {{ __('Submit') }}
                                </button>

                                <a href="{{ route('job.index') }}">
                                    <button type="button" class="btn btn-outline-danger">Back</button>
                                </a>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Add Job') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('job.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Job Title') }}</label>

                            <div class="col-md-6">
                                <input type="text" id="title" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" required>

                                @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Job Description') }}</label>

                            <div class="col-md-6">
                                <textarea id="description" rows="3" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}" required autocomplete="description" autofocus></textarea>

                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="technology" class="col-md-4 col-form-label text-md-right">{{ __('Technology') }}</label>

                            <div class="col-md-6">
                                <select id="technology" class="form-control @error('technology') is-invalid @enderror" name="technology" value="{{ old('technology') }}" required>
                                    <option value="">Select Technology</option>
                                    <option value="android">Android</option>
                                    <option value="c">C</option>
                                    <option value="ios">iOS</option>
                                    <option value="php">PHP</option>
                                    <option value="python">Python</option>
                                </select>

                                @error('technology')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="budget" class="col-md-4 col-form-label text-md-right">{{ __('Budget') }}</label>

                            <div class="col-md-6 input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">$</span>
                                </div>
                                <input id="budget" type="number" class="form-control @error('budget') is-invalid @enderror" name="budget" required autocomplete="current-budget" min="10" max="1000">

                                @error('budget')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="duration" class="col-md-4 col-form-label text-md-right">{{ __('Duration') }}</label>

                            <div class="col-md-6 input-group">
                                <input id="duration" type="number" class="form-control @error('duration') is-invalid @enderror" name="duration" required autocomplete="current-duration" min="1" max="30">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Days</span>
                                </div>

                                @error('duration')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-outline-primary">
                                    {{ __('Submit') }}
                                </button>

                                <a href="{{ route('job.index') }}">
                                    <button type="button" class="btn btn-outline-danger">Back</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection
