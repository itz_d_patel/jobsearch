@if(sizeof($workers) > 0)
    @foreach($workers as $worker)
        <div class="col-md-12">
            <div class="team d-md-flex p-4 bg-white">
            <div class="img" style="background-image: url(images/person_1.jpg);"></div>
            <div class="text pl-md-4">
                <!-- <span class="location mb-0">Western City, UK</span> -->
                <h2>{{ $worker->name }}</h2>
                <span class="position">{{ $worker->email }}</span>
                <!-- <p class="mb-2">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p> -->
                <!-- <span class="seen">Last Activity 4 months ago</span> -->
                <p><a href="{{ route('job.payment') }}" class="btn btn-primary">Hire</a></p>
            </div>
        </div>
    </div>
        <!-- <div class="col-md-4 mb-3">
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">{{ $worker->name }}</h5>
                    <p class="card-text">{{ $worker->email }}</p>
                    <a href="{{ route('job.payment') }}"><button type="button" class="btn btn-success">Hire</button></a>
                </div>
            </div>
        </div> -->
    @endforeach
@else
<div class="col-md-4 mb-3">
    <div class="alert alert-danger text-center" role="alert">
        Not found.
    </div>
</div>
@endif