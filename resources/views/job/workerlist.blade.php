@extends('layouts.app')

@section('content')
<div class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_1.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-end justify-content-start">
          <div class="col-md-12 ftco-animate text-center mb-5">
            <!-- <p class="breadcrumbs mb-0"><span class="mr-3"><a href="index.html">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>About</span></p> -->
            <h1 class="mb-3 bread">Hire Your Best Candidates</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section ftco-candidates ftco-candidates-2 bg-light">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 pr-lg-4">
                    <div class="row workerList">
                    </div>
                </div>
                <div class="col-lg-4 sidebar">
                <div class="sidebar-box bg-white p-4 ftco-animate">
                    <h3 class="heading-sidebar">Browse Category</h3>
                    <form action="#" class="search-form mb-3">
                <div class="form-group">
                  <span class="icon icon-search"></span>
                  <input type="text" id="searchWorkerText" class="form-control" placeholder="Search...">
                </div>
              </form>
                </div>
              </div>
            </div>
        </div>
    </section>
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Job Listing') }}</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <form class="form-inline mb-3">
                      <div class="form-group">
                        <label for="inputPassword6">Search: </label>
                        <input type="text" id="searchWorkerText" class="form-control mx-sm-3 searchWorkerText" placeholder="Search Worker">
                      </div>
                    </form>

                    <div class="row workerList">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection


@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        searchWroker();
    });
    
    $(document).on('keyup', '#searchWorkerText', function(e) {
        var searchText = $(this).val();
        searchWroker(searchText);
    });

    function searchWroker(text = '') {
        $.ajax({
            type: "POST",
            url: "{{ route('job.workerSearch') }}",
            data: {'text': text, _token: '{!! csrf_token() !!}'},
            cache: false,
            success: function(data)
            {
                $('.workerList').html(data);
            } 
        });
    }
</script>
@endpush

