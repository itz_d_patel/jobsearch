@extends('layouts.app')

@section('content')
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Home') }}</div>

                <div class="card-body">

                    {{ 'Hello ' . Auth::guard()->user()->name }}
            
                </div>
            </div>
        </div>
    </div>
</div> -->

<div class="hero-wrap img" style="background-image: url(images/bg_1.jpg);">
      <div class="overlay"></div>
      <div class="container">
        <div class="row d-md-flex no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-10 d-flex align-items-center ftco-animate">
                <div class="text text-center pt-5 mt-md-5">
                    <p class="mb-4">Find Job, Employment, and Career Opportunities</p>
                <h1 class="mb-5">The Eassiest Way to Get Your New Job</h1>
                            <div class="ftco-counter ftco-no-pt ftco-no-pb">
                    </div>
                            <div class="ftco-search my-md-5">
                                <div class="row">
                        <div class="col-md-12 nav-link-wrap">
                            <div class="nav nav-pills text-center" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                              <a class="nav-link active mr-md-1" id="v-pills-1-tab" href="{{ route('worker.jobList') }}" role="tab" aria-controls="v-pills-1" aria-selected="true">Find a Job</a>

                              <!-- <a class="nav-link" id="v-pills-2-tab" data-toggle="pill" href="#v-pills-2" role="tab" aria-controls="v-pills-2" aria-selected="false">Find a Candidate</a> -->

                            </div>
                          </div>
                        </div>
                    </div>
              </div>
            </div>
            </div>
      </div>
    </div>
@endsection
