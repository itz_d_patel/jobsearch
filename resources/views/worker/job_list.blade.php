@if(sizeof($jobs) > 0)
    @foreach($jobs as $job)
        <div class="col-md-12">
            <div class="job-post-item p-4 d-block d-lg-flex align-items-center">
              <div class="one-third mb-4 mb-md-0">
                <div class="job-post-item-header align-items-center">
                    <span class="subadge">{{ $job->jobtype }}</span>
                  <h2 class="mr-3 text-black"><a href="javascript:void(0);">{{ $job->title }}</a></h2>
                  <p>{{ $job->description }}</p>
                </div>
                <div class="job-post-item-body d-block d-md-flex">
                  <div class="mr-3"><span class="icon-layers"></span>{{ $job->user->name }}</div>
                  <div><span class="icon-desktop"></span> <span>{{ $job->technology }}</span></div>
                </div>
              </div>

              <div class="one-forth ml-auto d-flex align-items-center mt-4 md-md-0">
                <a href="job-single.html" class="btn btn-primary py-2">Apply Job</a>
              </div>
            </div>
          </div>
    @endforeach
@else
<div class="col-md-4 mb-3">
    <div class="alert alert-danger text-center" role="alert">
        Not found.
    </div>
</div>
@endif
