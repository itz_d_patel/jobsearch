@extends('layouts.app')

@section('content')
<div class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_1.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-end justify-content-start">
          <div class="col-md-12 ftco-animate text-center mb-5">
            <!-- <p class="breadcrumbs mb-0"><span class="mr-3"><a href="index.html">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>About</span></p> -->
            <h1 class="mb-3 bread">Browse Jobs</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section bg-light">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 pr-lg-4">
                    <div class="row jobList">
                    </div>
                </div>
                <div class="col-lg-3 sidebar">
                <div class="sidebar-box bg-white p-4 ftco-animate">
                    <h3 class="heading-sidebar">Browse Category</h3>
                    <form action="#" class="search-form mb-3">
                <div class="form-group">
                  <span class="icon icon-search"></span>
                  <input type="text" id="searchJobText" class="form-control" placeholder="Search...">
                </div>
              </form>
                </div>

                <div class="sidebar-box bg-white p-4 ftco-animate">
                    <h3 class="heading-sidebar">Job Type</h3>
                    <form action="#" class="browse-form">
                      <label for="option-job-type-1"><input type="checkbox" class="jobtypefilter" id="option-job-type-1" name="jobtype" value="parttime"> Partime</label><br>
                      <label for="option-job-type-2"><input type="checkbox" class="jobtypefilter" id="option-job-type-2" name="jobtype" value="fulltime"> Fulltime</label><br>
                      <label for="option-job-type-3"><input type="checkbox" class="jobtypefilter" id="option-job-type-3" name="jobtype" value="intership"> Intership</label><br>
                      <label for="option-job-type-4"><input type="checkbox" class="jobtypefilter" id="option-job-type-4" name="jobtype" value="temporary"> Temporary</label><br>
                      <label for="option-job-type-5"><input type="checkbox" class="jobtypefilter" id="option-job-type-5" name="jobtype" value="freelance"> Freelance</label><br>
                      <label for="option-job-type-6"><input type="checkbox" class="jobtypefilter" id="option-job-type-6" name="jobtype" value="fixed"> Fixed</label><br>
                    </form>
                </div>
              </div>
            </div>
        </div>
    </section>
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Jobs') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-inline mb-3">
                      <div class="form-group">
                        <label for="inputPassword6">Search: </label>
                        <input type="text" id="searchJobText" class="form-control mx-sm-3 searchJobText" placeholder="Search Job">
                      </div>
                    </form>

                    <div class="row jobList">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        searchJob();
    });
    var ajaxObject = {};
    
    $(document).on('keyup', '#searchJobText', function(e) {
        var searchText = $(this).val();
        ajaxObject.searchText = searchText;
        searchJob(ajaxObject);
    });

    $(document).on('click', '.jobtypefilter', function(e) {
        var jobtype = [];
        $.each($("input[name='jobtype']:checked"), function(){
            jobtype.push($(this).val());
        });
        ajaxObject.jobtype = jobtype;
        searchJob(ajaxObject);
    });

    function searchJob(ajaxObject = {}) {
        ajaxObject._token = '{!! csrf_token() !!}';
        $.ajax({
            type: "POST",
            url: "{{ route('worker.jobSearch') }}",
            data: ajaxObject,
            cache: false,
            success: function(data)
            {
                $('.jobList').html(data);
            } 
        });
    }
</script>
@endpush