<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::group(['middleware' => 'is_worker'], function()
{
	// Route::get('/', 'HomeController@index')->name('/');
	Route::get('/home', 'HomeController@index')->name('home');
	Route::post('jobSearch', 'HomeController@jobSearch')->name('worker.jobSearch');
	Route::get('joblist', 'HomeController@jobList')->name('worker.jobList');
});

Route::group(['middleware' => 'is_job'], function()
{
	Route::resource('/job', 'JobController');
	Route::post('workerSearch', 'JobController@workerSearch')->name('job.workerSearch');
	Route::get('workerlist', 'JobController@workerList')->name('job.workerlist');
	Route::get('payment', 'JobController@payment')->name('job.payment');
});




